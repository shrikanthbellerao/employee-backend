# employee-backend:
===================
This is the backend application which allows to manage employee. 
Framework       : Java & Spring Boot
Database type   : MongoDB
Database server : mLab (Cloud)
API methodology : REST
protocol        : http


Architecture (Scope: only the backend application):
---------------------------------------------------
I have implemented a decoupled approach where in I have 2 separate projects or applications:
1. Frontend application built using Angular 4 & Bootstrap
2. Backend application building using Java & Spring Boot/MongoDB

The benefit of decoupled approach is that the 2 applications are completely independent of each other in terms of:
- Technology or framework used to build
- Server on which the application runs etc.

Frontend application makes a REST API call to the Backend application. 
The API call is first handled by the EmployeeController present in the Backend application.
I am using the @RequestMapping of /employee @ Class level. This form the base URL http://localhost:8080/employee/
I have implemented 2 separate methods (fetchAllEmployees & addEmployee) to handle the 2 different functionalities
required to be handled by the application. Each of the methods in turn make use of the Model which depicts the 
collection's structure. EmployeeRepository interface is used as the connector or middle layer between the Model & 
the actual MongoDB collection.
I created a database called 'employee' in mLab (cloud).
Created a collection called 'employee' to store the data for all the employees.
After the response (JSON array) is received by the EmployeeController, it is finally returned to the frontend application.
Frontend application uses the data and displays it on screen.


Structure of the application:
-----------------------------
Root folder containing source file: /src/main/java/com/democompany/employee
- controller: EmployeeController.java
- models: Employee.java
- repositories: EmployeeRepository.java


Steps to compile & run the backend application:
-----------------------------------------------
Open command prompt & cd into the parent folder
Run this command: mvn spring-boot:run
This command creates the /target folder containing all the compiled class files


Steps to test the backend application (Standalone method):
---------------------------------------------------------
Configure the following URLs in postman:
1. http://localhost:8080/employee/fetchAllEmployees (Request type: GET)
    It should return JSON array containing all the employees with their info like first name, last name, gender, DOB & Department

2. http://localhost:8080/employee/addEmployee (Request type: POST)
    Request body should contain JSON object with the exact structure of the Employee model class:
    {
      "firstName": "Gary",
      "lastName": "Tomaka",
      "gender": "M",
      "dob" : "1970-04-25",
      "dept" : "A/P"
    }
    
    This API call will insert a document into employee collection present in MongoDB
    

Steps to test the backend application (via Frontend application):
-----------------------------------------------------------------
1. Access the frontend application via http://localhost:4200
2. You should now see all the employees displayed in a table. The list of employees was fetched by /fetchAllEmployees API
3. Fill the form in order to add a new employee. This uses /addEmployee API
4. You should now see the list of employees along with the recently added one as well