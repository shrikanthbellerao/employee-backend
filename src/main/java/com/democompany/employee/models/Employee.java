/*
 * Version Log
 * #------------#---------------#-------------#----------------------------------------------------------#
 * | JIRA       | Developer     | Date        | Comment                                                  |
 * #------------#---------------#-------------#----------------------------------------------------------#
 * | XXX        | Shrikanth Rao | 12-Oct-2018 | Initial Version                                          |
 * #------------#---------------#-------------#----------------------------------------------------------#
 * */
package com.democompany.employee.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/*
 * Model class for Employee collection present in MongoDB
 * */
public class Employee {

  @Id
  public ObjectId _id;
 
  public String firstName;
  public String lastName;
  public String gender;
  public String dob;
  public String dept;
 
  // Default Constructor
  public Employee() {
	// Nothing to do
  }
 
  // Parameterized Constructor
  public Employee(
    ObjectId _id, 
    String firstName, 
    String lastName, 
    String gender,
    String dob,
    String dept) {

	this._id 		= _id;
    this.firstName 	= firstName;
    this.lastName 	= lastName;
    this.gender 	= gender;
    this.dob 		= dob;
    this.dept		= dept;
  }
 
  // ObjectId needs to be converted to string
  public String get_id() { 
	return _id.toHexString(); 
  }
  public void set_id(ObjectId _id) {
    this._id = _id; 
  }
 
  // Getters & Setters for First Name
  public String getFirstName() {
	return firstName; 
  }
  public void setFirstName(String firstName) { 
	this.firstName = firstName; 
  }
 
  // Getters & Setters for Last Name
  public String getLastName() {
	return lastName; 
  }
  public void setLastName(String lastName) { 
	this.lastName = lastName; 
  }

  // Getters & Setters for Gender
  public String getGender() {
	return gender; 
  }
  public void setGender(String gender) { 
	this.gender = gender; 
  }
  
  // Getters & Setters for Date Of Birth
  public String getDob() {
	return dob; 
  }
  public void setDob(String dob) { 
	this.dob = dob; 
  }

  // Getters & Setters for Department
  public String getDept() {
	return dept; 
  }
  public void setDept(String dept) {
	this.dept = dept; 
  }
}