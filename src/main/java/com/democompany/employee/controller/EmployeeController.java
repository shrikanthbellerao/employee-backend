/*
 * Version Log
 * #------------#---------------#-------------#----------------------------------------------------------#
 * | JIRA       | Developer     | Date        | Comment                                                  |
 * #------------#---------------#-------------#----------------------------------------------------------#
 * | XXX        | Shrikanth Rao | 12-Oct-2018 | Initial Version                                          |
 * #------------#---------------#-------------#----------------------------------------------------------#
 * */
package com.democompany.employee.controller;

import com.democompany.employee.models.Employee;
import com.democompany.employee.repositories.EmployeeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.bson.types.ObjectId;
import javax.validation.Valid;
import java.util.List;

/*
 * Controller class to handle the following functionalities:
 * 1. Fetch all employees present in MongoDB
 * 2. Add a new employee to MongoDB
 * */
@RestController
@RequestMapping("/employee")
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeeController {

  @Autowired
  private EmployeeRepository repository;
  
  /*
   * Controller method to handle /fetchAllEmployees request mapping URL.
   * This calls a method present in respository to fetch all the employees 
   * present in MongoDB.
   * */
  @RequestMapping(value = "/fetchAllEmployees", method = RequestMethod.GET)
  public List<Employee> fetchAllEmployees() {
    System.out.print("\n Inside fetchAllEmployees");
    return repository.findAll();
  }
 
  /*
   * Controller method to handle /addEmployee request mapping URL.
   * This calls a method present in respository to add/insert a document 
   * containing info for a new Employee.
   * */
  @RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
  public List<Employee> createEmployee(@Valid @RequestBody Employee employee) {
	System.out.print("\n Inside createEmployee");
	System.out.print(employee.firstName);
	System.out.print(employee.lastName);
	employee.set_id(ObjectId.get());
    repository.save(employee);
    return fetchAllEmployees();
  }
}