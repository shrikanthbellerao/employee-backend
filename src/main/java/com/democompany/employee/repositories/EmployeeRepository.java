/*
 * Version Log
 * #------------#---------------#-------------#----------------------------------------------------------#
 * | JIRA       | Developer     | Date        | Comment                                                  |
 * #------------#---------------#-------------#----------------------------------------------------------#
 * | XXX        | Shrikanth Rao | 12-Oct-2018 | Initial Version                                          |
 * #------------#---------------#-------------#----------------------------------------------------------#
 * */
package com.democompany.employee.repositories;

import com.democompany.employee.models.Employee;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

/*
 * Repository class for Employee collection present in MongoDB
 * */
public interface EmployeeRepository extends MongoRepository<Employee, String> {
  Employee findBy_id(ObjectId _id);
}